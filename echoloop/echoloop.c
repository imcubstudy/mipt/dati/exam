#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>
#include <unistd.h>
#include <sys/select.h>
#include <fcntl.h>

#define FIFONAME    "/tmp/echoloop.fifo"

#define PROJID      42
#define SLEEPTIME   (1000 * 1000)

typedef struct {
    char    *str;
    int     lgth;
} arg_t;
arg_t arg = { .str = NULL, .lgth = 0 };
void cleanup(void) __attribute__((destructor()));
void getarg(int argc, char *argv[]);

void initsems(char *execname);
typedef enum {
    PRIMARY,
    SECONDARY
} instance_t;
instance_t checkinstance(char *execname);

void primary(char *path) __attribute__((noreturn));
void secondary(char *path) __attribute((noreturn));

int main(int argc, char *argv[])
{
    getarg(argc, argv);
    initsems(argv[0]);

    switch(checkinstance(argv[0])) {
        case PRIMARY:
            primary(FIFONAME);
        case SECONDARY:
            secondary(FIFONAME);
    }
}

#define _str(x) #x
#define str(x) _str(x)
#define try(code...)                                                \
    do {                                                            \
        errno = 0;                                                  \
        code;                                                       \
        if(errno != 0) {                                            \
            perror(#code " at " str(__LINE__));                     \
            exit(EXIT_FAILURE);                                     \
        }                                                           \
    } while(0)

void getarg(int argc, char *argv[])
{
    if(argc < 2) {
        fprintf(stderr, "Wrong amount of arguments\n"
                        "\tUsage: %s [args]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int total = 0, len[argc];
    for(int i = 1; i < argc; ++i) {
        len[i] = 1 + (int)strlen(argv[i]);
        total += len[i];
    }

    try(arg.str  = (char*)calloc(total, sizeof(char)));

    for(int i = 1; i < argc; ++i) {
        strcpy(arg.str + arg.lgth, argv[i]);
        arg.lgth += len[i];
        arg.str[arg.lgth - 1] = ' ';
    }
}

void cleanup()
{
    free(arg.str);
}

enum semnum_t {
    INSTANCE = 0,
    FIFO_READY,
    SECONDARY_POOL,
    NSEMS
};

#define MAXOPS  10
struct sembuf semopbuf[MAXOPS] = {};
int sem_id = -1;
int semopidx = 0;
#define semop_push(which, semop, flag)                                      \
    do {                                                                    \
        semopbuf[semopidx].sem_num  = which;                                \
        semopbuf[semopidx].sem_op   = semop;                                \
        semopbuf[semopidx].sem_flg  = flag;                                 \
        semopidx++;                                                         \
    } while(0)

#define semop_flush(...) ({                                                 \
        int     _semopidx   = semopidx;                                     \
        semopidx            = 0;                                            \
        int     _res        = semop(sem_id, semopbuf, _semopidx);           \
        _res;                                                               \
    })

void initsems(char *execname)
{
    key_t key = -1;
    try(key = ftok(execname, PROJID));
    try(sem_id = semget(key, NSEMS, IPC_CREAT | 0666));
}

instance_t checkinstance(char *execname)
{
    semop_push(INSTANCE, 0, IPC_NOWAIT);
    semop_push(INSTANCE, 1, SEM_UNDO);
    if(0 > semop_flush() && errno != EAGAIN) {
        perror("checkinstance");
        exit(EXIT_FAILURE);
    } else if(errno == EAGAIN) {
        errno = 0;
        return SECONDARY;
    } else {
        return PRIMARY;
    }
}

/////////////////////////////////////////////////////////////////////////////////
int getrdfifo(char *path);

int ispending(int fifo_fd);
void updatearg(int fifo_fd);

void primary(char *path)
{
    int fifo_fd = getrdfifo(path);

    semop_push(FIFO_READY, 1, SEM_UNDO);
    try(semop_flush()); // fifo is ready

    while(1) {
        while(ispending(fifo_fd))
            updatearg(fifo_fd);

        printf("%s\n", arg.str);

        usleep(SLEEPTIME);
    }
}

int getrdfifo(char *path)
{
    unlink(path);
    try(mkfifo(path, 0666));

    int fifo_fd = -1;
    try(fifo_fd = open(path, O_RDWR));

    return fifo_fd;
}

int ispending(int fifo_fd)
{
    fd_set set;
    FD_ZERO(&set);
    FD_SET(fifo_fd, &set);
    struct timeval tv = {};

    try(select(fifo_fd + 1, &set, NULL, NULL, &tv));
    return FD_ISSET(fifo_fd, &set);
}

void updatearg(int fifo_fd)
{
    int addlen = 0;
    try(read(fifo_fd, &addlen, sizeof(addlen)));

    char *tmps = NULL;
    try(tmps = calloc(arg.lgth + addlen + 1, sizeof(char)));
    memcpy(tmps, arg.str, arg.lgth);

    int rd = 0;

    while(rd < addlen) {
        try(rd += read(fifo_fd, tmps + arg.lgth + rd, addlen - rd));
    }

    free(arg.str);
    arg.str = tmps;
    arg.lgth += addlen;
}
/////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////
int getwrfifo(char *path);

void secondary(char *path)
{
    semop_push(SECONDARY_POOL, 0, 0);
    semop_push(SECONDARY_POOL, 1, SEM_UNDO);
    try(semop_flush());

    int fifo_fd = getwrfifo(path);

    try(write(fifo_fd, &arg.lgth, sizeof(arg.lgth)));

    int wr = 0;
    while(wr < arg.lgth) {
        try(wr += write(fifo_fd, arg.str + wr, arg.lgth - wr));
    }

    close(fifo_fd);

    semop_push(SECONDARY_POOL, -1, SEM_UNDO);
    try(semop_flush());

    printf("finished for %s\n", arg.str);

    exit(EXIT_SUCCESS);
}

int getwrfifo(char *path)
{
    semop_push(FIFO_READY, -1, SEM_UNDO);
    semop_push(FIFO_READY,  1, SEM_UNDO);
    try(semop_flush());

    int fifo_fd = -1;
    try(fifo_fd = open(path, O_WRONLY | O_NONBLOCK));
    try(fcntl(fifo_fd, F_SETFL, O_WRONLY));

    return fifo_fd;
}
/////////////////////////////////////////////////////////////////////////////////
