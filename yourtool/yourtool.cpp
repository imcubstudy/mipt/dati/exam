#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <errno.h>
#include <list>

typedef struct {
    char    *str;
    int     len;
} arg_t;
arg_t *getargs(int argc, char **argv);

char pullch(void);
void pushch(char c);
void flushbuf(void);

typedef struct {
    arg_t   *arg;
    int     cmpr;
} search_unit_t;
void spawnsu(char c, std::list<search_unit_t> &units, arg_t *args);
void burnsu(std::list<search_unit_t> &units);
void proceed(char c, std::list<search_unit_t> &units);

int main(int argc, char **argv)
{
    atexit(flushbuf);
    arg_t *args = getargs(argc, argv);

    std::list<search_unit_t> units;

    char c = '\0';
    while((c = pullch())) {
        pushch(c);
        spawnsu(c, units, args);
        proceed(c, units);
        burnsu(units);
    }

    delete[] args;
    return 0;
}

arg_t *getargs(int argc, char **argv)
{
    arg_t  *args = new arg_t[argc];

    int i = 0;
    for(i = 0; i < argc - 1; ++i) {
        args[i].str  = argv[i + 1];
        args[i].len  = (int)strlen(argv[i + 1]);
    }
    args[argc].str = nullptr;

    return args;
}

void spawnsu(char c, std::list<search_unit_t> &units, arg_t *args)
{
    for(int i = 0; args[i].str != nullptr; ++i) {
        if(c == args[i].str[0]) {
            search_unit_t su = {
                .arg    = &args[i],
                .cmpr   = 0
            };
            units.push_back(su);
        }
    }
}
void burnsu(std::list<search_unit_t> &units)
{
    units.remove_if([=](search_unit_t su) { return su.arg == NULL; });
}

void proceed(char c, std::list<search_unit_t> &units)
{
    for(auto &su: units) {
        if(c == su.arg->str[su.cmpr]) {
            if(su.arg->len == ++su.cmpr) {
                pushch('*');
                su.arg = NULL;
            }
        } else {
            su.arg = NULL;
        }
    }
}

#define BUFSZ   4096
typedef struct {
    char    data[BUFSZ];
    int     used;
} iobuf_t;
iobuf_t input     = { .data = {}, .used = BUFSZ };
iobuf_t output    = { .data = {}, .used = 0 };

char pullch(void)
{
    if(input.used >= BUFSZ) {
        int r = -1;
        if(0 > (r = (int)read(STDIN_FILENO, input.data, BUFSZ))) {
            perror("input err");
            exit(EXIT_FAILURE);
        }

        if(r < BUFSZ)
            input.data[r] = '\0';

        input.used = 0;
    }
    return input.data[input.used++];
}

void pushch(char c)
{
    if(output.used == BUFSZ)
        flushbuf();

    output.data[output.used++] = c;
}

void flushbuf(void)
{
    if(0 > write(STDOUT_FILENO, output.data, output.used)) {
        perror("output err");
        exit(EXIT_FAILURE);
    }
    output.used = 0;
}
