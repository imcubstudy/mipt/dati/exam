#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef enum {
    EMPTY = 0,
    X = 1,
    O = 2
} cell_t;

int nwins(cell_t *grid, cell_t target);
int isgame(cell_t *grid);
void getfirstgame(cell_t *grid);
int getnextgame(cell_t *grid);
int isequal(cell_t *a, cell_t *b);

typedef struct {
    cell_t  **set;
    int     size;
} unique_gameset_t;
void pushgame(unique_gameset_t *set, cell_t *grid);

int isgame(cell_t *grid)
{
    int nX = 0, nO = 0;
    for(int i = 0; i < 9; ++i) {
        if(grid[i] == X) nX++;
        if(grid[i] == O) nO++;
    }
    if(!((nX >= nO) && (nX - nO <= 1))) {
        return 0;
    } else {
        cell_t target = (nX == nO) ? O : X;
        return nwins(grid, target) >= 0;
    }
}

int iswinrow(cell_t *grid, int a, int b, int c, cell_t target)
{
    return grid[a] == target &&
           grid[a] == grid[b] &&
           grid[a] == grid[c];
}

int nwins(cell_t *grid, cell_t target)
{
    int nX = 0, nO = 0;
    for(int i = 0; i < 9; ++i) {
        if(grid[i] == X) nX++;
        if(grid[i] == O) nO++;
    }

    if(nX < 3 && nO < 3) {
        return 0;
    }

    cell_t poisontarget = (target == X) ? O : X;
    int res = 0;

    // rows
    for(int i = 0; i < 3; ++i) {
        res += iswinrow(grid, 3 * i, 3 * i + 1, 3 * i + 2, target);
        res -= iswinrow(grid, 3 * i, 3 * i + 1, 3 * i + 2, poisontarget) * 100;
    }

    // columns
    for(int i = 0; i < 3; ++i) {
        res += iswinrow(grid, i, 3 + i, 6 + i, target);
        res -= iswinrow(grid, i, 3 + i, 6 + i, poisontarget) * 100;
    }

    // diag
    res += iswinrow(grid, 0, 4, 8, target);
    res -= iswinrow(grid, 0, 4, 8, poisontarget) * 100;
    res += iswinrow(grid, 2, 4, 6, target);
    res -= iswinrow(grid, 2, 4, 6, poisontarget) * 100;

    return res;
}

int decr(cell_t *grid)
{
    for(int i = 0; i < 9; ++i) {
        if(grid[i] != 0) {
            grid[i]--;
            for(int j = 0; j < i; ++j)
                grid[j] = 2;
            return 1;
        }
    }
    return 0;
}

int getnextgame(cell_t *grid)
{
    do {
        if(!decr(grid))
            return 0;
    } while(!isgame(grid));

    return 1;
}

int isequal_(cell_t *a, cell_t *b)
{
    int res = 1;
    for(int i = 0; res && i < 9; ++i) {
        res = (a[i] == b[i]);
    }

    return res;
}

int transforms[][9] = {
    {6, 7, 8, 3, 4, 5, 6, 7, 8}, // horisontal flip
    {2, 1, 0, 5, 4, 3, 8, 7, 6}, // vertical flip
    {0, 3, 6, 1, 4, 7, 2, 5, 8}, // diag 1
    {8, 5, 2, 7, 4, 1, 6, 3, 0}, // diag 2
    {2, 5, 8, 1, 4, 7, 0, 3, 6}, // rotate 90
    {8, 7, 6, 5, 4, 3, 2, 1, 0}, // rotate 180
    {6, 3, 0, 7, 4, 1, 8, 5, 2}, // rotate 270
};

void transform(cell_t *grid, int map[9])
{
    cell_t t[9] = {};
    memcpy(t, grid, 9 * sizeof(cell_t));
    for(int i = 0; i < 9; ++i) {
        grid[i] = t[map[i]];
    }
}

int isequal(cell_t *a, cell_t *b)
{
    int res = 0;

    res = isequal_(a, b);
    if(res) return res;

    cell_t dummy[9] = {};

    for(int i = 0; i < sizeof(transforms)/sizeof(transforms[0]); ++i) {
        memcpy(dummy, b, 9 * sizeof(cell_t));
        transform(dummy, transforms[i]);

        res = isequal_(a, dummy);
        if(res) return res;
    }

    return res;
}

void pushgame(unique_gameset_t *set, cell_t *grid)
{
    for(int i = 0; i < set->size; ++i) {
        if(isequal(set->set[i], grid))
            return;
    }

    set->set[set->size] = calloc(9, sizeof(cell_t));
    memcpy(set->set[set->size], grid, 9 * sizeof(cell_t));

    set->size++;
}

void getfirstgame(cell_t *grid)
{
    for(int i = 0; i < 9; ++i) {
        grid[i] = 2;
    }
    getnextgame(grid);
}

int main()
{
    cell_t grid[9] = {};
    getfirstgame(grid);

    int total = 0;
    do {
        total++;
    } while(getnextgame(grid));
    printf("total possible games %d\n", total);

    getfirstgame(grid);
    unique_gameset_t set = {
        .set = calloc(total, sizeof(cell_t*)),
        .size = 0
    };

    do {
        pushgame(&set, grid);
    } while(getnextgame(grid));
    printf("unique games %d\n", set.size);

    for(int i = 0; i < set.size; ++i) {
        free(set.set[i]);
    }
    free(set.set);

    return 0;
}
