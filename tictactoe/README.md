# 2. Крестики-нолики.

Оцените как можно точнее число возможных полей, возникающих в игре крестики-нолики 3×3. Приведите решение.

## Решение

Зададим состояние игрового поля с помощью числа в троичной системе со следующим соответствием цифр и содержимого ячейки:
```
typedef enum {
    EMPTY = 0,
    X = 1,
    O = 2
} cell_t;
```
Само троичное число можно объявить массивом из девяти ячеек: `cell_t grid[9] = {};`

Теперь определим необходимое условие для того, чтобы поле соответстовало игровой ситуации. Очевидно, что функцию проверки можно задать следующим образом (первый ход за Х):
```
int isgame(cell_t *grid)
{
    int nX = 0, nO = 0;
    for(int i = 0; i < 9; ++i) {
        if(grid[i] == X) nX++;
        if(grid[i] == O) nO++;
    }

    return ((nX >= nO) && (nX - nO <= 1)) && nwins(grid) < 2;
}
```

Теперь нетрудно получить игровую ситуацию:
```
void getfirstgame(cell_t *grid)
{
    for(int i = 0; i < 9; ++i) {
        grid[i] = 2;
    }
    getnextgame(grid);
}

int getnextgame(cell_t *grid)
{
    do {
        if(!decr(grid)) // <- декремент троичного представления
            return 0;
    } while(!isgame(grid));

    return 1;
}
```

Найдем количество всевозможных игровых ситуаций:
```
cell_t grid[9] = {};
getfirstgame(grid);

int total = 0;
do {
    total++;
} while(getnextgame(grid));
printf("total possible games %d\n", total);
```
#### Получаем `> total possible games 6046`

---

#### Теперь учтем всевозможные симметрии игрового поля:
1. Симметрия отн-но вертикальной оси
1. Симметрия отн-но горизонтальной оси
1. Повороты поля на 90, 180, 270 градусов
1. Симметрии отн-но диагоналей

Все эти преобразования поля задаются перестановками цифр представления игрового поля троичным числом
```
int transforms[][9] = {
    {6, 7, 8, 3, 4, 5, 6, 7, 8}, // horisontal flip
    {2, 1, 0, 5, 4, 3, 8, 7, 6}, // vertical flip
    {0, 3, 6, 1, 4, 7, 2, 5, 8}, // diag 1
    {8, 5, 2, 7, 4, 1, 6, 3, 0}, // diag 2
    {2, 5, 8, 1, 4, 7, 0, 3, 6}, // rotate 90
    {8, 7, 6, 5, 4, 3, 2, 1, 0}, // rotate 180
    {6, 3, 0, 7, 4, 1, 8, 5, 2}, // rotate 270
};

void transform(cell_t *grid, int map[9])
{
    cell_t t[9] = {};
    memcpy(t, grid, 9 * sizeof(cell_t));
    for(int i = 0; i < 9; ++i) {
        grid[i] = t[map[i]];
    }
}
```
Проверка игровых ситуаций на идентичность с учетом симметрий игрового поля:
```
int isequal_(cell_t *a, cell_t *b)
{
    int res = 1;
    for(int i = 0; res && i < 9; ++i) {
        res = (a[i] == b[i]);
    }

    return res;
}

int isequal(cell_t *a, cell_t *b)
{
    int res = 0;

    res = isequal_(a, b);
    if(res) return res;

    cell_t dummy[9] = {};

    for(int i = 0; i < sizeof(transforms)/sizeof(transforms[0]); ++i) {
        memcpy(dummy, b, 9 * sizeof(cell_t));
        transform(dummy, transforms[i]);

        res = isequal_(a, dummy);
        if(res) return res;
    }

    return res;
}
```
Теперь мы можем забрутфорсить задачу, благо всего возможных ситуаций немного - 3^9
Работа фунции `pushgame` тривиальна по своей логике - см. исходник
```
getfirstgame(grid);
unique_gameset_t set = {
    .set = calloc(total, sizeof(cell_t*)),
    .size = 0
};

do {
    pushgame(&set, grid);
} while(getnextgame(grid));
printf("unique games %d\n", set.size);
```
#### Получаем `> unique games 1513`

#### UPD (13.05.19):
В вышеизложенном решении были опущены: условие победы игрока только после своего хода; возможность одним ходом закрыть сразу две, например, диагонали. Исправленная версия дает результат:
```
total possible games 5478
unique games 1374
```
